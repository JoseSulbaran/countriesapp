import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";

// =======  HOME  ====== //
import { Home, NotFound404 } from '../pages';

export default function Routers(props) {
  return  (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route render={ NotFound404 } />
      </Switch>
    </Router>
  )
}

