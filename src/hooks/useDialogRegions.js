// Dependencies
import { useState, useEffect, useCallback } from "react";
import { Region } from '../graphql/regions.query'
import apollo from '../lib/apollo'


export default function (props) {

  const [stateScreen, setStateScreen] = useState({
    regions:[], subregions:[], filter_region : "", filter_subregions: ""
  });

  useEffect(() => {
    (async () => {
      await handleNextFetch({options:"GET_REGIONS"})
    })();
	}, [props]);

  useEffect(() => {
			const { regions, filter_region } = stateScreen
			let resul = regions.filter(items => items.name === filter_region )[0]
			if (resul){
				handleForm({ subregions: resul.subregions })
			}
  }, [stateScreen.filter_region]);

  const handleNextFetch = useCallback( async _props => {
    const { options, ...res } = _props
    let data = null
    switch (options) {
      case "GET_REGIONS":
        const { errors,  data } = await apollo.query({
          query: Region,
        })
        if (data?.Region){
            handleForm({ regions: data.Region });
        }
        break;
      default:
        return null;
        break;
    }
  }, [stateScreen]);


  const handleForm = useCallback(_props => {
    setStateScreen((state) => ({ ...state, ..._props }));
  }, [stateScreen]);


  return {
    ...stateScreen,
    handleForm
  };
}
