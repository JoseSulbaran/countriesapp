// Dependencies
import { useState, useEffect, useCallback } from "react";

import { 
Country, 
Country_Filter_Name, 
Country_Filter_Code, 
Country_Filter_Currency,
Country_Filter_Region,
Country_Filter_Subregion, 
} from '../graphql/country.query'
import { Language_Filter_Lang } from "../graphql/language.query"
import apollo from '../lib/apollo'

export default function (props) {

  const [stateScreen, setStateScreen] = useState({
    listCountry:[], search :"", showModal : false, items:{}, loading:true, 
    showlanguages:false, showcurrencies:false, showregions:false
  });

  useEffect(() => {
    (async () => {
      await handleNextFetch({options:"GET_COUNTRY"})
    })();
  }, []);
  

  const handleNextFetch = useCallback( async _props => {
    const { options, ...res } = _props
    let data = null
    switch (options) {
      case "GET_COUNTRY":
        const { errors,  data } = await apollo.query({
          query: Country,
        })
        if (data?.Country){
          setStateScreen((state) => ({ ...state, listCountry: data.Country, loading:false }));
        }
        break;
      case "GET_COUNTRY_FILTER":
        let _listCountry = []
        const { ...name } = await apollo.query({
          query: Country_Filter_Name,
          variables: {
            search: res.search
          },
        })

        const { ...code } = await apollo.query({
          query: Country_Filter_Code,
          variables: {
            alpha2Code: res.search,
          }
        })

        if( name?.data.Country?.length > 0 ){
          _listCountry = await name.data.Country
        } else if( code?.data.Country?.length > 0 ){
          _listCountry =  await code.data.Country
        } 
        setStateScreen((state) => ({ ...state, listCountry: _listCountry }));
        break;
      case "GET_COUNTRY_FILTER_CURRENCY":
        const { ...curr } = await apollo.query({
          query: Country_Filter_Currency,
          variables: {
            code: res.filter_code
          },
        })
        if (curr?.data?.Country) setStateScreen((state) => ({ ...state, listCountry: curr.data.Country, showcurrencies:false  }));
        break;
      case "GET_COUNTRY_FILTER_SUBREGION":
        const { ...sub } = await apollo.query({
          query: Country_Filter_Subregion,
          variables: {
            name_subregion : res.filter_subregions,
            name_region: res.filter_region
          },
        })
        if (sub?.data?.Country) setStateScreen((state) => ({ ...state, listCountry: sub.data.Country, showregions:false  }));
        break;
      case "GET_COUNTRY_FILTER_REGION":
        const { ...reg } = await apollo.query({
          query: Country_Filter_Region,
          variables: {
            name_region: res.filter_region
          },
        })
        if (reg?.data?.Country) setStateScreen((state) => ({ ...state, listCountry: reg.data.Country, showregions:false  }));
        break;
      case "GET_COUNTRY_FILTER_LANGUAGES":
        let _list = []
        const { ...lagg } = await apollo.query({
          query: Language_Filter_Lang,
          variables: {
            name_lang: res.filter_lang
          },
        })
        if (lagg?.data?.Language.length > 0) {
          let _items = lagg.data.Language[0]
          if (_items) _list = _items.countries
        } 
        setStateScreen((state) => ({ ...state, listCountry: _list, showlanguages:false  }));
        break;
      default:
        return null;
        break;
    }
  }, [stateScreen]);


  const handleForm = useCallback(_props => {
    const { search  } = _props
    if ( search?.length > 0 ){
      handleNextFetch({options:"GET_COUNTRY_FILTER", search})
    } else if( search?.length === 0 ){
      handleNextFetch({options:"GET_COUNTRY"})
    }
    setStateScreen((state) => ({ ...state, ..._props }));
  }, [stateScreen]);

  const handleOpenFilter = useCallback(_props => {
    const { filter,  } = _props
    if (filter === "0"){
      handleForm({search:""})
    } else if( filter === "1" ){
      handleForm({showlanguages:true})
    } else if( filter === "2" ){
      handleForm({showcurrencies:true})
    } else if( filter === "3" ){
      handleForm({showregions:true})
    }
  }, [stateScreen]);

  const handleFilter = useCallback(_props => {
    const { filter_code, filter_region, filter_subregions, filter_lang  } = _props

    if ((filter_code && filter_code === "-1") || (filter_region && filter_region === "-1") || (filter_lang && filter_lang === "")){
      handleNextFetch({options:"GET_COUNTRY"})
    } else {
      if (filter_code){
        handleNextFetch({options:"GET_COUNTRY_FILTER_CURRENCY", filter_code })
      } else if (filter_region &&  filter_subregions?.length > 0 ){
        handleNextFetch({options:"GET_COUNTRY_FILTER_SUBREGION", filter_region, filter_subregions })
      } else if (filter_region){
        handleNextFetch({options:"GET_COUNTRY_FILTER_REGION", filter_region })
      } else if (filter_lang){
        handleNextFetch({options:"GET_COUNTRY_FILTER_LANGUAGES", filter_lang })
      }
    }

  }, [stateScreen]);

  return {
    handleForm,
    handleOpenFilter,
    handleFilter,
    ...stateScreen,
  };
}
