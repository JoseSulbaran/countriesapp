// Dependencies
import { useState, useEffect, useCallback } from "react";
import { Language } from '../graphql/language.query'
import apollo from '../lib/apollo'


export default function (props) {

  const [stateScreen, setStateScreen] = useState({
    language:[], filter_lang:""
  });

  useEffect(() => {
    (async () => {
      await handleNextFetch({options:"GET_LANGUAGE"})
    })();
  }, [props]);

  const handleNextFetch = useCallback( async _props => {
    const { options, ...res } = _props
    let data = null
    switch (options) {
      case "GET_LANGUAGE":
        const { errors,  data } = await apollo.query({
          query: Language,
        })
        if (data?.Language){
          console.log({ errors,  data })

          setStateScreen((state) => ({ ...state, language: data.Language }));
        }
        break;
      default:
        return null;
        break;
    }
  }, [stateScreen]);

  const handleForm = useCallback(_props => {
    setStateScreen((state) => ({ ...state, ..._props }));
  }, [stateScreen]);

  return {
    ...stateScreen,
    handleForm
  };
}
