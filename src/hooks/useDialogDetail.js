// Dependencies
import { useState, useEffect } from "react";
import { Country_Detail } from '../graphql/country.query'
import apollo from '../lib/apollo'


export default function (props) {

  const [stateScreen, setStateScreen] = useState({
    country:{}
  });

  useEffect(() => {
    (async () => {
      const { items } = props
      if ( items?._id ) {
        const { errors,  data } = await apollo.query({
          query: Country_Detail,
          variables: {
            _id: items._id
          },
        })
        if (data?.Country){
          setStateScreen((state) => ({ ...state, country: data.Country[0] }));
        }
      }
    })();
  }, [props]);
  
  return {
    ...stateScreen,
  };
}
