// Dependencies
import { useState, useEffect, useCallback } from "react";
import { Currencies } from '../graphql/currencies.query'
import apollo from '../lib/apollo'


export default function (props) {

  const [stateScreen, setStateScreen] = useState({
    currencies:[], filter_code : ""
  });

  useEffect(() => {
    (async () => {
      await handleNextFetch({options:"GET_CURRENCIES"})
    })();
  }, [props]);

  const handleNextFetch = useCallback( async _props => {
    const { options, ...res } = _props
    let data = null
    switch (options) {
      case "GET_CURRENCIES":
        const { errors,  data } = await apollo.query({
          query: Currencies,
        })
        if (data?.Currency){
            handleForm({ currencies: data.Currency });
        }
        break;
      default:
        return null;
        break;
    }
  }, [stateScreen]);


  const handleForm = useCallback(_props => {
    setStateScreen((state) => ({ ...state, ..._props }));
  }, [stateScreen]);

  return {
    ...stateScreen,
    handleForm
  };
}
