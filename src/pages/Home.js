


import React, { useCallback } from 'react';
import useHome  from '../hooks/useHome'
import DialogDetail  from '../components/DialogDetail'
import DialogLanguages  from '../components/DialogLanguages'
import DialogCurrencies  from '../components/DialogCurrencies'
import DialogRegions  from '../components/DialogRegions'

export default function Home(props) {
  const { listCountry, search,  handleForm, handleOpenFilter, handleFilter,  ...res  } = useHome(props);

  const getLanguages = useCallback( lag => {
    let resp = ""
    resp = (
      <table className="border-collapse table-auto w-full whitespace-no-wrap bg-white table-striped relative">
        <tbody className="divide-y">
          {(lag || []).map((val, key) => 
            <tr key={key}> 
              <td className="border-r border-l text-left text-primary">- {val.name}</td>
            </tr>
          )}
        </tbody>
      </table>
    )
    return resp
  }, [listCountry]);

  return (
    <div className="">
      { res.showModal && <DialogDetail showModal={res.showModal} callback={handleForm} items={res.items}/>}
      { res.showlanguages && <DialogLanguages showlanguages={res.showlanguages} callback={handleForm} handleFilter={handleFilter}/>}
      { res.showcurrencies && <DialogCurrencies showcurrencies={res.showcurrencies} callback={handleForm} handleFilter={handleFilter}/>}
      { res.showregions && <DialogRegions showregions={res.showregions} callback={handleForm} handleFilter={handleFilter}/>}

      <div className="bg-secondary p-2 md:p-10 lg:p-10 ">
        <div className="bg-secondary relative text-gray-600  container mx-auto focus-within:text-gray-400">
          <span className="absolute inset-y-0 left-0 flex items-center pl-2">
            <button type="submit" className="p-1 focus:outline-none focus:shadow-outline"
              onClick={() =>  handleForm({ search })}
            >
              <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" className="w-6 h-6"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
            </button>
          </span>
          <input
            id="search"
            placeholder="Search..."
            autoComplete="off"
            value={search}
            onChange={({ target: { value } }) => handleForm({ search: value })}           
             //className="block w-full px-4 py-2 mx-auto font-mono leading-5 text-white placeholder-opacity-50 bg-transparent border border-white rounded-lg md:flex-1 focus:outline-none focus:ring-2 focus:ring-primary focus:ring-offset-2 focus:ring-offset-secondary"
            className="w-full pl-10 pr-10 px-3 py-2 placeholder-gray-500 transition-all duration-150 bg-white rounded focus:outline-none focus:bg-white focus:shadow-outline"
          />
          {search.length > 0 &&
            <span className="absolute inset-y-0 right-0 flex items-center pr-3">
              <button className="items-center focus:outline-none focus:shadow-outline"
                onClick={() =>  handleForm({ search: "" })}
              >
                <h5 className="font-size-12 font-bold text-center">x</h5>
              </button>
            </span>
          }
        </div> 
      </div>

      <div className=" flex flex-colp-10 md:p-5 container mx-auto p-2  ">
          <h2 className=" text-4xl font-bold text-secondary">Países</h2>
      </div>

      <div className=" flex justify-end container mx-auto p-2">
        <div className="mb-5 text-right">
          <p className="mb-3 text-lg ">Filtrar por:</p>
          <div className="relative inline-block w-64">
            <select
              onChange={({ target: { value } }) => handleOpenFilter({filter : value})}
              className="block w-full px-4 py-2 pr-8 leading-tight bg-transparent border border-gray-400 rounded shadow appearance-none hover:border-gray-500 focus:outline-none focus:shadow-outline"
            >
              <option value="0">Mostrar todos</option>
              <option value="1">Idiomas</option>
              <option value="2">Currency</option>
              <option value="3">Región</option>
            </select>
            <div className="absolute inset-y-0 right-0 flex items-center px-2 text-gray-700 pointer-events-none">
              <svg className="w-4 h-4 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-colp-10 container mx-auto p-2">
        <div className="flex-grow overflow-y-hidden overflow-x-auto ">
          <table className="relative w-full border">
            <thead>
              <tr>
                <th className=" w-1/2 px-6 py-3  bg-gray-300">Nombre</th>
                <th className=" top-0 px-6 py-3  bg-gray-300">Native</th>
                <th className=" top-0 px-6 py-3  bg-gray-300">Alpha2Code</th>
                <th className=" top-0 px-6 py-3  bg-gray-300">Capital</th>
                <th className=" top-0 px-6 py-3  bg-gray-300">Currencies</th>
                <th className=" top-0 px-6 py-3  bg-gray-300">Subregion</th>
                <th className=" top-0 px-6 py-3  bg-gray-300">Region</th>
                <th className=" top-0 px-6 py-3  bg-gray-300">Languages</th>
                <th className=" top-0 px-6 py-3  bg-gray-300"></th>
              </tr>
            </thead>
            <tbody className="divide-y">
              {(listCountry || []).map((items, key) => (
                <tr key={key}>
                  <td className="px-6 py-4 text-left"> {items.name}</td>
                  <td className="px-6 py-4 text-left"> {items.nativeName}</td>
                  <td className="px-6 py-4 text-left"> {items.alpha2Code}</td>
                  <td className="px-6 py-4 text-left"> {items.capital}</td>
                  <td className="px-6 py-4 text-left"> {getLanguages(items.currencies)}</td>

                  <td className="px-6 py-4 text-left"> {items?.subregion?.name}</td>
                  <td className="px-6 py-4 text-left"> {items?.subregion?.region?.name}</td>
                  <td className="px-6 py-4 text-left"> {getLanguages(items.officialLanguages)}</td>
                  <td className="px-6 py-4 text-left">
                  <button
                    type="submit"
                    className="px-4 py-2 rounded-lg text-secondary bg-primary focus:ring-2 focus:ring-primary focus:ring-offset-2 focus:ring-offset-secondary focus:outline-none"
                    onClick={() => handleForm({showModal:true, items})}
                  >
                    <svg fill="none" className="h-7 text-dark"  viewBox="0 0 24 24" stroke="currentColor">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                    </svg>
                  </button>
                  </td>
                </tr>
              ))}

            </tbody>
          </table>
          {listCountry.length === 0 && !res.loading &&
              <div className=" px-10 mt-20 mb-20 mx-auto">
                <h5 className=" text-1xl font-bold text-center text-secondary">Sin resultado...</h5>
              </div>
          }
          {res.loading &&
              <div className=" px-10 mt-20 mb-20 py-50 mx-auto">
                <h5 className=" text-1xl font-bold text-center text-secondary">Cargando...</h5>
              </div>
          }
      </div>
    </div>
      <div className="mt-20" />

  </div>
  );
};

