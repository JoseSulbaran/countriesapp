import React from 'react';

export default function NotFound404(props) {
  return  (
    <center><br/>
      <h3>404 Pagina no encontrada</h3>
      <p>Lo sentimos, pero la página que busca no existe.</p>
    </center>
  )
}

