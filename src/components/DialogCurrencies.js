import React from "react";
import useDialogCurrencies  from '../hooks/useDialogCurrencies'

export default function (props) {
  const {showcurrencies, callback, handleFilter} = props;
  const { currencies, handleForm, filter_code } = useDialogCurrencies(props);
  return (
    <div>
      {showcurrencies && (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-hidden  fixed inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative w-auto my-6 mx-auto  max-w-6xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                  <h3 className="text-3xl font-semibold">
                    Currency
                  </h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-1 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => callback && callback({showcurrencies:false})}
                  >
                    <span className=" text-black">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                   <div className="pr-10 flex">
                    <div className=" ">
                      <div className="relative inline-block w-92">
                        <select
                          onChange={e => handleForm({filter_code : e.target.value})}
                          className="block w-full px-4 py-2 pr-8 leading-tight bg-transparent border border-gray-400 rounded shadow appearance-none hover:border-gray-500 focus:outline-none focus:shadow-outline"
                        >
                          <option value="-1">Mostrar todos</option>
                          {currencies.map((items, key) => items.code !== "(none)" && items.code !== "null" &&

                            <option key={key} value={items.code}>{items.code} - {items.name} </option>
                          )}
                        </select>
                        <div className="absolute inset-y-0 right-0 flex items-center px-2 text-gray-700 pointer-events-none">
                          <svg className="w-4 h-4 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                          </svg>
                        </div>
                      </div>
                    </div>
                  </div>
      
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end p-6 border-t border-solid border-gray-300 rounded-b">
                  <button
                    className="bg-gray-400 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}
                    onClick={() => callback && callback({showcurrencies:false})}
                  >
                    Cerrar
                  </button>
                  <button
                    className="bg-green-400 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}
                    onClick={() => handleFilter({ filter_code })}
                  >
                    Filtrar
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      )}
    </div>
  );
}