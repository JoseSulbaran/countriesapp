import React, { useCallback } from "react";
import useDialogDetail  from '../hooks/useDialogDetail'

export default function DialogDetail(props) {
  const {showModal, callback} = props;
  const { country } = useDialogDetail(props);

  const getLang = useCallback( lag => {
    let resp = ""
    resp = (
      <table className="border-collapse table-auto w-full whitespace-no-wrap bg-white table-striped relative">
        <tbody className="divide-y">
          {(lag || []).map((val, key) => 
            <tr key={key}> 
              <td className="border-r border-l text-left text-primary">- {val.name}</td>
            </tr>
          )}
        </tbody>
      </table>
    )
    return resp
  }, [country]);

  return (
    <div>
      {showModal && (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-hidden  fixed inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative w-auto my-6 mx-auto  max-w-6xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                  <h3 className="text-3xl font-semibold">
                    Detalle de país
                  </h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-1 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => callback && callback({showModal:false})}
                  >
                    <span className=" text-black">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <div className="flex-grow h-96 w-72 md:w-full lg:w-full overflow-x-auto  overflow-y-auto ">
                    {Object.keys(country).length === 0 &&
                      <div className="">
                        <h5 className=" text-1xl font-bold text-center text-secondary">Cargando...</h5>
                      </div>
                    }
                    <table className="relative w-full border">
                      <tbody className="divide-y">
                        {country?._id && 
                          <>
                            <tr> 
                              <td className="text-left text-secondary">ID</td>
                              <td className="border-r text-left">{country._id}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Nombre</td>
                              <td className="border-r text-left">{country.name}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Native</td>
                              <td className="border-r  text-left">{country.nativeName}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">emoji</td>
                              <td className="border-r  text-left">{country?.flag?.emoji}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Alpha2Code</td>
                              <td className="border-r  text-left">{country.alpha2Code}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Alpha3Code</td>
                              <td className="border-r  text-left">{country.alpha3Code}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Capital</td>
                              <td className="border-r  text-left">{country.capital}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Subregion</td>
                              <td className="border-r  text-left">{country?.subregion?.name}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Region</td>
                              <td className="border-r  text-left">{country?.subregion?.region?.name}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Currencies</td>
                              <td className="border-r  text-left">{getLang(country.currencies)}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Languages</td>
                              <td className="border-r  text-left">{getLang(country.officialLanguages)}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Area</td>
                              <td className="border-r  text-left">{country?.area}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Demonym</td>
                              <td className="border-r  text-left">{country?.demonym}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Gini</td>
                              <td className="border-r  text-left">{country?.gini}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Population Density</td>
                              <td className="border-r  text-left">{country?.populationDensity}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Population</td>
                              <td className="border-r  text-left">{country?.population}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Alternative Spellings</td>
                              <td className="border-r  text-left">{getLang(country.alternativeSpellings)}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Regional Blocs</td>
                              <td className="border-r  text-left">{getLang(country.regionalBlocs)}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">area</td>
                              <td className="border-r  text-left">{country?.area}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">TopLevelDomains</td>
                              <td className="border-r  text-left">{getLang(country.topLevelDomains)}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Latitude</td>
                              <td className="border-r  text-left">{country?.location?.latitude}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">Longitude</td>
                              <td className="border-r  text-left">{country?.location?.longitude}</td>
                            </tr>
                            <tr> 
                              <td className="text-left text-secondary">numericCode</td>
                              <td className="border-r  text-left">{country?.numericCode}</td>
                            </tr>
                          </>
                        }
                      </tbody>
                    </table>
                  </div>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end p-6 border-t border-solid border-gray-300 rounded-b">
                  <button
                    className="bg-gray-400 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}
                    onClick={() => callback && callback({showModal:false})}
                  >
                    Cerrar
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      )}
    </div>
  );
}