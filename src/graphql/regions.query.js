// Dependencies
import { gql } from '@apollo/client'

export const Region = gql`
  query {
    Region {
      name
      subregions (orderBy: name_asc) {
        name
      }
    }
  }
`;
