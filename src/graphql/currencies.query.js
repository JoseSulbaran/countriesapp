// Dependencies
import { gql } from '@apollo/client'

export const Currencies = gql`
  query {
    Currency (orderBy: code_asc) {
      name
      code
      symbol
      countries {
        name
      }
    }
  }
`;
