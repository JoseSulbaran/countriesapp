// Dependencies
import { gql } from '@apollo/client'

export const Country = gql`
  query  {
    Country (orderBy: name_asc)  {
      _id
      name
      nativeName
      alpha2Code
      capital
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      currencies {
        name
        symbol
      }
      officialLanguages {
        name
      }
    }
  }
`;

export const Country_Filter_Name = gql`
  query search($search: String) {
    Country(name: $search, orderBy: name_asc) {
      _id
      name
      nativeName
      alpha2Code
      capital
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      currencies {
        name
        symbol
      }
    }
  }
`;

export const Country_Filter_Code = gql`
  query search($alpha2Code: String) {
    Country(alpha2Code: $alpha2Code) {
      _id
      name
      nativeName
      alpha2Code
      capital
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      currencies {
        name
        symbol
      }
      officialLanguages {
        name
      }
    }
  }
`;

export const Country_Detail = gql`
  query search($_id: String) {
    Country(_id: $_id) {
      _id
      name
      nativeName
      alpha2Code
      alpha3Code
      area
      population
      populationDensity
      capital
      demonym
      gini
      location {
        latitude
        longitude
      }
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      officialLanguages {
        iso639_1
        iso639_2
        name
        nativeName
      }
      currencies {
        name
        symbol
      }
      regionalBlocs {
        name
        acronym
        otherAcronyms {
          name
        }
        otherNames {
          name
        }
      }
      flag {
        emoji
        emojiUnicode
        svgFile
      }
      topLevelDomains {
        name
      }
      callingCodes {
        name
      }
      alternativeSpellings {
        name
      }
    }
  }
`;


export const Country_Filter_Currency = gql`
  query currency($code: String) {
    Country(filter : {
      currencies: {
       code: $code
     }}, orderBy: name_asc) {
      _id
      name
      nativeName
      alpha2Code
      capital
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      currencies {
        name
        symbol
      }
      officialLanguages {
        name
      }
    }
  }
`;


export const Country_Filter_Subregion = gql`
  query currency($name_subregion: String, $name_region: String) {
    Country(filter : {
      subregion: {
        name : $name_subregion,
        region: {
          name: $name_region
        }
     }}, orderBy: name_asc) {
      _id
      name
      nativeName
      alpha2Code
      capital
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      currencies {
        name
        symbol
      }
      officialLanguages {
        name
      }
    }
  }
`;


export const Country_Filter_Region = gql`
  query currency($name_region: String) {
    Country(filter : {
      subregion: {
        region: {
          name: $name_region
        }
     }}, orderBy: name_asc) {
      _id
      name
      nativeName
      alpha2Code
      capital
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      currencies {
        name
        symbol
      }
      officialLanguages {
        name
      }
    }
  }
`;


export const Country_Filter_Lag = gql`
  query currency($name_Lag: String) {
    Country(filter : {
      subregion: {
        region: {
          name: $name_region
        }
     }}, orderBy: name_asc) {
      _id
      name
      nativeName
      alpha2Code
      capital
      numericCode
      subregion {
        name
        region {
          name
        }
      }
      currencies {
        name
        symbol
      }
      officialLanguages {
        name
      }
    }
  }
`;