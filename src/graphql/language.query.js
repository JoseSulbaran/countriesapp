// Dependencies
import { gql } from '@apollo/client'

export const Language = gql`
  query {
    Language (orderBy: name_asc) {
      name
      nativeName
    }
  }
`;



export const Language_Filter_Lang = gql`
  query lag($name_lang: String) {
    Language(name : $name_lang) {
      iso639_1
      iso639_2
      name
      nativeName
      countries (orderBy: name_asc) {
        _id
        name
        nativeName
        alpha2Code
        capital
        numericCode
        subregion {
          name
          region {
            name
          }
        }
        currencies {
          name
          symbol
        }
        officialLanguages {
          name
        }
      }
    }
  }
`;