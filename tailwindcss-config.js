module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        aspectRatio: {
          none: 0,
          square: [1, 1],
          '16/9': [16, 9],
          '4/3': [4, 3],
          '21/9': [21, 9],
        },
        backdropFilter: {
          none: `none`,
          'blur-0': 'blur(0px)',
          'blur-px': 'blur(1px)',
          'blur-0.5': 'blur(0.125rem)',
          'blur-1': 'blur(0.25rem)',
          'blur-1.5': 'blur(0.375rem)',
          'blur-2': 'blur(0.5rem)',
          'blur-2.5': 'blur(0.625rem)',
          'blur-3': 'blur(0.75rem)',
          'blur-3.5': 'blur(0.875rem)',
          'blur-4': 'blur(1rem)',
          'blur-5': 'blur(1.25rem)',
          'blur-6': 'blur(1.5rem)',
        },
        borderRadius: {
          '2xl': `2rem`,
        },
        borderWidth: {
          10: `10px`,
        },
        colors: {
          primary: `#00C389`,
          secondary: `#071D49`,
          tertiary: `#2F80ED`,
        },

    },
  },
  variants: {
    extend: {
      tableLayout: ['hover', 'focus'],

    },
  },
  plugins: [],
  corePlugins: {
    // ...
   tableLayout: false,
  }
}
